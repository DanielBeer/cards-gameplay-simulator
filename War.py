from Game import *
from Deck import Card
from math import pi as PI

class War(Game):
	def play(self, n):
		print('Round ' + str(n))
		
		winner = self.get_winner()
		
		if winner > 0:			
			for card in self.players[0].cards:
				dx, dy = 400 - card.cr_obj.x, 300 - card.cr_obj.y
				
				card.cr_obj.move(int(dx), int(dy), 10)
			
			return n
		else:
			self.round([], n)

	def play_cards(self):
		cards = []		
		num_left = len(self.players)
		
		for p in self.players:
			card = p.play_card()
		
			if card:
				card.cr_obj.state = 1
				card.cr_obj.update()
			
				print('Player ' + str(p.id) + ' flips a ' + str(Card.lookup(card.value)))
			
				cards.append(card)
		
		if len(self.players) == 1: # move win check
			return
			
		return cards
	
	def get_round_winner_index(self, cards):		
		values = list(map(lambda c : c.value, cards))
	
		if values.count(max(values)) == 1:
			i = values.index(max(values))
			
			print('Player ' + str(self.players[i].id) + ' wins the round')
			
			return i
		else:		
			return -1
			
	def get_winner(self):
		if len(self.players) == 1:
			return self.players[0].id
		else:
			return 0
	
	def reset_posns(self, ):
		new_player_num = len(self.players)
		new_angle = 2 * PI / new_player_num
				
		for i in range(len(self.players)):
			p = self.players[i]
		
			for card in p.cards:
				dx, dy = rotate_vec2(0, -200, new_angle * i)

				dx -= card.cr_obj.x - 400
				dy -= card.cr_obj.y - 300
				
				card.cr_obj.move(int(dx), int(dy), 10)
				card.cr_obj.resize(int(56 / (1 + 0.2 * max(new_player_num - 4, 0))), int(90 / (1 + 0.2 * max(new_player_num - 4, 0))), 10)
	
	def check_cards(self, cards, winnings, n):
		i = self.get_round_winner_index(cards)

		if i == -1:
			print('It\'s a war')
			
			hidden = self.play_cards()
		
			if not hidden:
				return
			
			for card in hidden:
				card.cr_obj.state = 0
				card.cr_obj.move(int(card.cr_obj.hw * 2.2), 0, 5)
			
			cards += hidden
			
			self.round(cards + winnings, n)
		else:
			new = cards + winnings
			
			random.SystemRandom().shuffle(new)
			
			if (len(self.players[i].cards) == 0):
				moves = []
				
				top_card_cr_obj = None
			
				for card in new:				
					dx, dy = rotate_vec2(0, -200, 2 * PI / len(self.players) * i)
					dx -= card.cr_obj.x - 400
					dy -= card.cr_obj.y - 300

					card.cr_obj.state = 0
					
					if dx == 0 and dy == 0:
						top_card_cr_obj = card.cr_obj
					else:
						moves.append((card.cr_obj, int(dx), int(dy)))

				for mv in moves:
					mv[0].move(mv[1], mv[2], 10)
			else:
				for card in new:				
					dx, dy = rotate_vec2(0, -200, 2 * PI / len(self.players) * i)
					
					dx -= card.cr_obj.x - 400
					dy -= card.cr_obj.y - 300

					card.cr_obj.state = 0
										
					top_card_cr_obj = self.players[i].cards[-1].cr_obj
					
					card.cr_obj.move(int(dx), int(dy), 10, onIter=lambda : top_card_cr_obj.update())				
			
			self.players[i].cards = new + self.players[i].cards
			
			out = []
			
			for p in self.players:
				if len(p.cards) == 0:
					print('Player ' + str(p.id) + ' is out of cards')
				
					out.append(p)
			
			for p in out:
				self.players.remove(p)
			
			self.canvas.after(15 * int(1000 / FPS), lambda : self.reset_posns())
			self.canvas.after(80 * int(1000 / FPS), lambda : self.play(n + 1))
	
	def round(self, winnings, n):
		for p in self.players:
			print('Player ' + str(p.id) + '\'s deck: ' + ', '.join(list(map(lambda c : str(Card.lookup(c.value)), p.cards))))
			pass
	
		cards = self.play_cards()
		
		if not cards:
			return
		
		self.canvas.after(80 * int(1000 / FPS), lambda : self.check_cards(cards, winnings, n))