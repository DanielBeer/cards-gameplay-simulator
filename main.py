import gc
from tkinter import font
from tkinter import *

from War import War
from Deck import *

root = Tk()
root.title('Auto War')

canvas = Canvas(root, width=800, height=600, bg='green')
canvas.pack()

def update_loop(f=None):
	if callable(f):
		f()
	elif type(f) is tuple:	
		for g in f:
			g()
	
	canvas.after(int(1000 / FPS), lambda : update_loop(f=f))

n = int(input('How many players? '))

deck = []
card_texts = [str(i) for i in (range(2, 11))] + ['J', 'Q', 'K', 'A']

for i in range(13):
	deck.append(Card(Card.SUIT_HEARTS, card_texts[i], i, root=root, canvas=canvas))
	deck.append(Card(Card.SUIT_SPADES, card_texts[i], i, root=root, canvas=canvas))
	deck.append(Card(Card.SUIT_DIAMONDS, card_texts[i], i, root=root, canvas=canvas))
	deck.append(Card(Card.SUIT_CLUBS, card_texts[i], i, root=root, canvas=canvas))
	
DECK_52 = Deck(deck, canvas=canvas, root=root, n_players=n)

game = War(deck=DECK_52, canvas=canvas)
game.start(n)

update_loop()

root.mainloop()