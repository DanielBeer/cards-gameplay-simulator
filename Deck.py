from Render import *
from tkinter import font
import math, random

from Game import FPS

class Suit:
	def __init__(self, colour, img_path):
		self.colour = colour
		self.img_path = img_path

class Card:
	SUIT_HEARTS = Suit('red', 'resources/heart.png')
	SUIT_SPADES = Suit('black', 'resources/spade.png')
	SUIT_DIAMONDS = Suit('red', 'resources/diamond.png')
	SUIT_CLUBS = Suit('black', 'resources/club.png')
	
	VISIBILITY_BACK = 0
	VISIBILITY_FRONT = 1
	
	def __init__(self, suit, text, value, root=None, canvas=None):
		self.suit, self.text, self.value, self.root, self.canvas = suit, str(text), value, root, canvas

	def lookup(value):
		return [2, 3, 4, 5, 6, 7, 8, 9, 10, 'J', 'Q', 'K', 'A'][value]

	class RenderObject(CRObject):
		def __init__(self, x, y, hw, hh, suit, value, rotation=0, visibility_state=None, canvas=None, root=None):
			self.hw, self.hh = hw, hh
			
			self.canvas, self.root = canvas, root
		
			offsets = (x, y, rotation)
			fs = int(min(hw / 4, hh / 4))
			f = font.Font(root=self.root, size=fs, family='Comic Sans MS')
			i = int(min(hw / 30, hh / 30))
			img_w = int(0.7 * fs)
			cr = int(min(hw / 12, hh / 12))
		
			elements = [CRRect(0, 0, hw, hh, 0, corner_r=cr, outline='black', fill='white',
							visibility=(Card.VISIBILITY_BACK, Card.VISIBILITY_FRONT), offsets=offsets, canvas=canvas),
						CRRect(0, 0, hw - i, hh - i, 1, corner_r=cr, outline='red', fill='white',
							visibility=(Card.VISIBILITY_FRONT,), offsets=offsets, canvas=canvas),
						CRRect(0, 0, hw - 2 * i, hh - 2 * i, 2, corner_r=cr, outline='red', fill='red',
							visibility=(Card.VISIBILITY_BACK,), offsets=offsets, canvas=canvas),
						CRImage(-hw + 4 * i + img_w / 2 + 2, -hh + 8 * i + img_w + fs / 2 + 2, suit.img_path, 2, w=img_w, h=img_w,
							visibility=(Card.VISIBILITY_FRONT,), canvas=canvas),
						CRImage(hw - 4 * i - img_w / 2 - 2, hh - 8 * i - img_w - fs / 2 - 2, suit.img_path, 2, rotation=math.pi, w=img_w, h=img_w,
							visibility=(Card.VISIBILITY_FRONT,), canvas=canvas),
						CRText(-hw + 4 * i + img_w / 2 + 2, -hh + 4 * i + img_w / 2 + 2, Card.lookup(value), suit.colour, 2, font=f,
							visibility=(Card.VISIBILITY_FRONT,), offsets=offsets, canvas=canvas),
						CRText(hw - 4 * i - img_w / 2 - 2, hh - 4 * i - img_w / 2 - 2, Card.lookup(value), suit.colour, 2, font=f, rotation=math.pi,
							visibility=(Card.VISIBILITY_FRONT,), offsets=offsets, canvas=canvas)]
		
			super(Card.RenderObject, self).__init__(x, y, rotation=rotation, elements=elements, state=visibility_state or Card.VISIBILITY_BACK, canvas=canvas)
		
		def resize(self, hw, hh, frames):
			ix, iy = (hw - self.hw) / frames, (hh - self.hh) / frames
					
			def local_resize(frames):
				self.hw += ix
				self.hh += iy
			
				fs = int(min(self.hw / 4, self.hh /4))
				f = font.Font(root=self.root, size=fs, family='Comic Sans MS')
				i = int(min(self.hw / 30, self.hh / 30))
				img_w = int(0.7 * fs)
				cr = int(min(self.hw / 12, self.hh / 12))

				r1, r2, r3 = self.elements[0], self.elements[1], self.elements[2]
				r1.hw, r1.hh, r1.r = self.hw, self.hh, cr
				r2.hw, r2.hh, r2.r = self.hw - i, self.hh - i, cr
				r3.hw, r3.hh, r3.r = self.hw - 2 * i, self.hh - 2 * i, cr
				
				i1, i2 = self.elements[3], self.elements[4]
				i1.x, i1.y, i1.w, i1.h = -self.hw + 4 * i + img_w / 2 + 2, -self.hh + 8 * i + img_w + fs / 2 + 2, img_w, img_w
				i2.x, i2.y, i2.w, i2.h = self.hw - 4 * i - img_w / 2 - 2, self.hh - 8 * i - img_w - fs / 2 - 2, img_w, img_w
				
				t1, t2 = self.elements[5], self.elements[6]
				t1.x, t1.y = -self.hw + 4 * i + img_w / 2 + 2, -self.hh + 4 * i + img_w / 2 + 2
				t2.x, t2.y = self.hw - 4 * i - img_w / 2 - 2, self.hh - 4 * i - img_w / 2 - 2
				
				self.canvas.itemconfig(t1.id, font=f)
				self.canvas.itemconfig(t2.id, font=f)
			
				self.update()
			
				if frames > 1:
					self.canvas.after(int(1000 / FPS), lambda : local_resize(frames - 1))
					
			local_resize(frames)
			
		def move(self, dx, dy, frames, onIter=None):
			if dx == 0 and dy == 0:
				return
		
			prevx, prevy = self.x, self.y
			ix, iy = dx / frames, dy / frames
			
			def local_move(frames):
				self.x += ix
				self.y += iy
				
				self.update()

				if frames > 1:
					if onIter:
						onIter()
					
					self.canvas.after(int(1000 / FPS), lambda : local_move(frames - 1))
				else:
					self.x = round(self.x)
					self.y = round(self.y)

			local_move(frames)
			
		def map_to_elements(self, f):
			for element in self.elements:
				f(element)
						
		def to_top(self):
			self.map_to_elements(canvas.tag_raise)
			
		def to_bottom(self):
			self.map_to_elements(canvas.tag_lower)

class Deck:
	def __init__(self, cards, canvas=None, root=None, n_players=0):
		self.cards = cards
		
		for c in self.cards:
			c.cr_obj = Card.RenderObject(400, 300, int(56 / (1 + 0.2 * max(n_players - 4, 0))), int(90 / (1 + 0.2 * max(n_players - 4, 0))),
				c.suit, c.value, visibility_state=Card.VISIBILITY_BACK, canvas=canvas, root=root)