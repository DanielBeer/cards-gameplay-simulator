import random, math, time

FPS = 60

def rotate_vec2(x, y, angle):
	cs = math.cos(angle)
	sn = math.sin(angle)

	return round(x * cs - y * sn), round(x * sn + y * cs)

class Game:
	def __init__(self, deck=None, shuffle=True, canvas=None):
		self.deck = deck
		
		if shuffle:
			random.SystemRandom().shuffle(self.deck.cards)
			
		self.canvas = canvas
		
	def start(self, n_players):
		self.n_players = n_players
		self.deal(n_players)
	
	def deal(self, n_players):
		self.players = []
		
		for i in range(n_players):
			self.players.append(Player(i + 1))

		angle_inc = 2 * math.pi / n_players
				
		def deal_anim(i):
			if len(self.deck.cards) > 0:
				next_card = self.deck.cards.pop()
				
				dx, dy = rotate_vec2(0, -200, angle_inc * i)
				
				next_card.cr_obj.move(int(dx), int(dy), 10)
			
				self.players[i].cards.append(next_card)
				
				if i < n_players - 1:
					self.canvas.after(int(5 * 1000 / FPS), lambda : deal_anim(i + 1))
				else:
					self.canvas.after(int(5 * 1000 / FPS), lambda : deal_anim(0))
					
				return
			
			self.canvas.after(int(60 * 1000 / FPS), lambda : self.play(1))

		deal_anim(0)
	
	def round(self, n):
		pass
	
	def get_winner(self):
		return 1
		
	def play(self, n):
		print('Round ' + str(n))
		
		winner = self.get_winner()
		
		if winner > 0:
			print('Winner is Player ' + str(winner))
			
			for card in self.players[0].cards:
				dx, dy = 400 - card.cr_obj.x, 300 - card.cr_obj.y
				
				card.cr_obj.move(dx, dy, 10)
			
			return n
		else:
			self.round(n)
		
			self.canvas.after(160 * int(1000 / FPS), lambda : self.play(n + 1))

class Player:
	def __init__(self, id):
		self.cards = []
		self.id = id
		
	def play_card(self):
		if len(self.cards) > 0:
			return self.cards.pop()
		else:
			return False