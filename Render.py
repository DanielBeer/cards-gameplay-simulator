from tkinter import *
from PIL import Image, ImageTk
import math
from Game import rotate_vec2

class CanvasRenderedObject:
	def __init__(self, x, y, rotation=0, elements=[], state=None, canvas=None, root=None):
		self.x = x
		self.y = y
		self.rot = rotation + math.pi
		self.elements = sorted(elements, key=(lambda v : v.z))
		self.state = state
		self.canvas = canvas
		self.root = root
		
		self.update()
	
	def hide(self):
		for v in self.elements:
			v.hide()
	
	def show(self):
		for v in self.elements:
			v.show()
		
	def update(self):	
		self.elements.sort(key=(lambda v : v.z))
	
		for v in self.elements:		
			if self.state in v.visibility:
				self.canvas.itemconfig(v.id, state=NORMAL)
				v.update(self)
			else:
				self.canvas.itemconfig(v.id, state=HIDDEN)
	
CRObject = CanvasRenderedObject

class CanvasRenderedElement:
	def __init__(self, id, x, y, z, rotation=0, visibility=(), canvas=None):	
		self.id = id
		self.x = x
		self.y = y
		self.z = z
		self.rot = rotation
		self.visibility = visibility
		self.canvas = canvas
		
	def hide(self):
		self.canvas.itemconfig(id, state=HIDDEN)
		
	def show(self):
		self.canvas.itemconfig(id, state=NORMAL)
		
	def rotate_coords(self, coords, angle, parent=None):
		n = int(len(coords) / 2)
		
		shape_x, shape_y = self.x, self.y
		center_x, center_y = shape_x, shape_y
		
		if parent:		
			offset_x, offset_y = rotate_vec2(shape_x, shape_y, angle)
			center_x = parent.x  - offset_x
			center_y = parent.y - offset_y

		mapped_coords = []

		for i in range(n * 2):
			c = coords[i]
			
			if i % 2 == 0:
				mapped_coords.append(c - shape_x)
			else:
				mapped_coords.append(c - shape_y)

		new_coords = []
		
		for i in range(n):
			x, y = rotate_vec2(mapped_coords[2 * i], mapped_coords[2 * i + 1], angle)

			new_coords.append(center_x + x)
			new_coords.append(center_y + y)

		return new_coords
		
	def get_transform_coords(self, parent):
		coords = self.canvas.coords(self.id)
		new_coords = []
	
		for i in range(int(len(coords) / 2)):
			new_coords.append(parent.x + coords[2 * i])
			new_coords.append(parent.y + coords[2 * i + 1])
			
		return new_coords
		
	def update(self, parent):		
		x, y = rotate_vec2(self.x, self.y, parent.rot)
	
		self.canvas.coords(self.id, x + parent.x, y + parent.y)
		self.canvas.tag_raise(self.id)

CRElement = CanvasRenderedElement
	
class CanvasRenderedImage(CRElement):
	imgs = []

	def __init__(self, x, y, img_path, z, rotation=0, w=None, h=None, visibility=(), **kwargs):
		super(CanvasRenderedImage, self).__init__(-1, x, y, z, rotation=rotation, visibility=visibility, **kwargs)
		
		self.img_path = img_path
		self.w, self.h = w, h
				
	def update(self, parent):
		if self.id != -1:
			self.canvas.delete(self.id)
		
		img = Image.open(self.img_path)
		
		if self.w and self.h:
			img = img.resize((self.w, self.h), Image.ANTIALIAS)
		
		img = img.rotate((self.rot - parent.rot) * 180 / math.pi)
		
		img = ImageTk.PhotoImage(img)
		CanvasRenderedImage.imgs.append(img)
		
		x, y = rotate_vec2(self.x, self.y, parent.rot)
		
		self.id = self.canvas.create_image(x + parent.x, y + parent.y, image=img)
		self.canvas.tag_raise(self.id)
		
CRImage = CanvasRenderedImage

class CanvasRenderedRect(CRElement):
	def get_rounded_rect_coords(x, y, r, hw, hh):
		x1, y1, x2, y2 = x - hw, y - hh, x + hw, y + hh
		
		if r > 0:
			return [x1 + r, y1,
					x1 + r, y1,
					x2 - r, y1,
					x2 - r, y1,
					x2, y1,
					x2, y1 + r,
					x2, y1 + r,
					x2, y2 - r,
					x2, y2 - r,
					x2, y2,
					x2 - r, y2,
					x2 - r, y2,
					x1 + r, y2,
					x1 + r, y2,
					x1, y2,
					x1, y2 - r,
					x1, y2 - r,
					x1, y1 + r,
					x1, y1 + r,
					x1, y1]
		else:
			return [x1, y1, x2, y1, x2, y2, x1, y2]

	def __init__(self, x, y, hw, hh, z, rotation=0, corner_r=0, visibility=(), offsets=(0, 0, 0), canvas=None, **kwargs):	
		super(CanvasRenderedRect, self).__init__(canvas.create_polygon(*CanvasRenderedRect.get_rounded_rect_coords(x + offsets[0], y + offsets[1], corner_r, hw, hh),
																		smooth=True, **kwargs), x, y, z, rotation=rotation, visibility=visibility, canvas=canvas)

		self.hw = hw
		self.hh = hh
		self.r = corner_r
		
	def update(self, parent):
		if parent.rot + self.rot == 0:
			self.canvas.coords(self.id, CanvasRenderedRect.get_rounded_rect_coords(self.x, self.y, self.r, self.hw, self.hh))
			self.canvas.coords(self.id, *self.get_transform_coords(parent))
		else:		
			self.canvas.coords(self.id, *self.rotate_coords(CanvasRenderedRect.get_rounded_rect_coords(self.x, self.y, self.r, self.hw, self.hh), parent.rot + self.rot, parent=parent))
			
		self.canvas.tag_raise(self.id)

CRRect = CanvasRenderedRect

class CanvasRenderedText(CRElement):
	def __init__(self, x, y, text, colour, z, rotation=0, font=None, visibility=(), offsets=(0, 0, 0), canvas=None, **kwargs):
		rot = rotation + offsets[2]
	
		super(CanvasRenderedText, self).__init__(canvas.create_text(x + offsets[0], y + offsets[1], text=str(text), font=font or CanvasRenderedText.FONT_DEFAULT,
																	fill=colour, angle=rot * 180 / math.pi, **kwargs),
													x, y, z, rotation, visibility=visibility, canvas=canvas, **kwargs)
	
	def update(self, parent):
		self.canvas.itemconfig(self.id, angle=(self.rot - parent.rot) * 180 / math.pi)
		super(CanvasRenderedText, self).update(parent)

CRText = CanvasRenderedText